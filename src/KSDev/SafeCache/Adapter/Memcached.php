<?php

namespace KSDev\SafeCache\Adapter;


class Memcached implements AdapterInterface {
    public static $defaultMemcached;

    protected $prefix;
    protected $invalid_value = '__INVALID__';

    /** @var \Memcached $conn */
    protected $conn;

    public function __construct(\Memcached $memcached = null, $prefix = null) {
        if($memcached)
            $this->conn = $memcached;
        else {
            if(!self::$defaultMemcached) {
                self::$defaultMemcached = new \Memcached();
                self::$defaultMemcached->addServer('127.0.0.1', 11211);
            }

            $this->conn = self::$defaultMemcached;
        }
    }

    /**
     * @return \Memcached
     */
    public function getConnection() {
        return $this->conn;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        $value = $this->conn->get($prefix . $key, null, $cas_token);
        return (is_string($value) && strpos($value, $this->getInvalidValue()) === 0) ? null : $value;
    }

    /**
     * safe gets item - if not exists, then set through value or closure
     * @param $key
     * @param \Closure|mixed $set_value
     * @param null $ttl
     * @return mixed
     * @throws \Exception
     */
    public function getOrSet($key, $set_value, $tags = [], $ttl = null, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        if($tags && count($tags) > 0) {
            throw new \Exception("Tags is not supported by memcache");
        }

        $value = $this->conn->get($prefix . $key, null, $cas_token);
        if($this->conn->getResultCode() == \Memcached::RES_SUCCESS) {
            if(is_string($value) && strpos($value, $this->getInvalidValue()) === 0) {
                // Cache is invalided, use cas, to ensure nobody set invalid again (update) og set another value.
                $data = is_callable($set_value) ? $set_value($this) : $set_value;
                $this->conn->cas($cas_token, $prefix . $key, $data, $ttl);
                return $data;
            }
            return $value;
        } elseif($this->conn->getResultCode() == \Memcached::RES_NOTFOUND) {
            // Cache not exists, use $m->add, since if somebody else updates it, we want to disallow this update
            $data = is_callable($set_value) ? $set_value($this) : $set_value;
            $this->conn->add($prefix . $key, $data, $ttl);
            return $data;
        } else {
            throw new \Exception("Unknown cache error ".$this->conn->getResultMessage());
        }
    }

    public function forceSet($key, $value, $tags = [], $ttl = null, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        if($tags && count($tags) > 0) {
            throw new \Exception("Tags is not supported by memcache");
        }

        return $this->conn->set($prefix.$key, $value, $ttl);
    }


    /**
     * Safe invalidate item
     * @param $key
     */
    public function invalidateKey($key, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;
        $this->conn->set($prefix . $key, '__INVALID__');
    }


    /**
     * @param array $keys
     */
    public function invalidateKeys($keys, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;
        foreach($keys as $key)
            $this->conn->set($prefix . $key, '__INVALID__');
    }

    /**
     * @return mixed
     */
    public function getPrefix() {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     * @return $this
     */
    public function setPrefix($prefix) {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvalidValue() {
        return $this->invalid_value;
    }

    /**
     * @param string $invalid_value
     * @return $this
     */
    public function setInvalidValue($invalid_value) {
        $this->invalid_value = $invalid_value;
        return $this;
    }
}