<?php

namespace KSDev\SafeCache\Adapter;

interface AdapterInterface {
    /**
     * @return \Redis
     */
    public function getConnection();

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key, $prefix = null);

    /**
     * safe gets item - if not exists, then set through value or closure
     * @param $key
     * @param \Closure|mixed $set_value
     * @param null $ttl
     * @return mixed
     * @throws \Exception
     */
    public function getOrSet($key, $set_value, $tags = [], $ttl = null, $prefix = null);


    /**
     * Force set value
     * @param $key
     * @param $value
     * @param array $tags
     * @param int $ttl
     * @param null $prefix
     * @throws \Exception
     */
    public function forceSet($key, $value, $tags = [], $ttl = null, $prefix = null);

    /**
     * Safe invalidate item
     * @param $key
     */
    public function invalidateKey($key, $prefix = null);

    /**
     * @param array $keys
     */
    public function invalidateKeys($keys, $prefix = null);

    /**
     * @return mixed
     */
    public function getPrefix();

    /**
     * @param mixed $prefix
     * @return $this
     */
    public function setPrefix($prefix);

    /**
     * @return string
     */
    public function getInvalidValue();

    /**
     * @param string $invalid_value
     * @return $this
     */
    public function setInvalidValue($invalid_value);
}