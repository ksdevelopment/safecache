<?php

namespace KSDev\SafeCache\Adapter;


class Redis implements AdapterInterface {
    public static $defaultConnection;

    protected $prefix;
    protected $invalid_value = '__INVALID__';

    /** @var \Redis $conn */
    protected $conn;

    public function __construct(\Redis $conn = null, $prefix = null) {
        if ($conn)
            $this->conn = $conn;
        else {
            if (!self::$defaultConnection) {
                self::$defaultConnection = new \Redis();
                self::$defaultConnection->connect('127.0.0.1');
            }

            $this->conn = self::$defaultConnection;
        }
        $this->conn->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
    }

    /**
     * @return \Redis
     */
    public function getConnection() {
        return $this->conn;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        $value = $this->conn->get($prefix . $key);

        return (is_string($value) && strpos($value, $this->getInvalidValue()) === 0) ? null : $value;
    }

    /**
     * safe gets item - if not exists, then set through value or closure
     * @param $key
     * @param \Closure|mixed $set_value
     * @param null $ttl
     * @return mixed
     * @throws \Exception
     */
    public function getOrSet($key, $set_value, $tags = [], $ttl = null, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        if ($tags && count($tags) > 0) {
            throw new \Exception("Tags is not supported by memcache");
        }

        $this->conn->watch($prefix . $key);
        $value = $this->conn->get($prefix . $key);
        if ($value !== false) {
            if (is_string($value) && strpos($value, $this->getInvalidValue()) === 0) {
                // Cache is invalided, use cas, to ensure nobody set invalid again (update) og set another value.
                $data = is_callable($set_value) ? $set_value($this) : $set_value;
                $this->conn->multi();
                $this->conn->set($prefix . $key, $value, $ttl);
                $this->conn->exec();

                return $data;
            }

            return $value;
        } else {
            // Cache not exists, use conn->setnx, since if somebody else updates it, we want to disallow this update (memcached->add equality)
            $data = is_callable($set_value) ? $set_value($this) : $set_value;

            $options = ['nx'];
            if ($ttl)
                $options['ex'] = $ttl;

            $this->conn->set($prefix . $key, $data, $options);

            return $data;
        }
    }

    /**
     * Force set value
     * @param $key
     * @param $value
     * @param array $tags
     * @param int $ttl
     * @param null $prefix
     * @throws \Exception
     */
    public function forceSet($key, $value, $tags = [], $ttl = null, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;

        if ($tags && count($tags) > 0) {
            throw new \Exception("Tags is not supported by memcache");
        }

        $prefix = $prefix === null ? $this->getPrefix() : $prefix;
        return $this->conn->set($prefix . $key, $value, $ttl);
    }

    /**
     * Safe invalidate item
     * @param $key
     */
    public function invalidateKey($key, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;
        $this->conn->set($prefix . $key, '__INVALID__');
    }


    /**
     * @param array $keys
     */
    public function invalidateKeys($keys, $prefix = null) {
        $prefix = $prefix === null ? $this->getPrefix() : $prefix;
        foreach ($keys as $key)
            $this->conn->set($prefix . $key, '__INVALID__');
    }

    /**
     * @return mixed
     */
    public function getPrefix() {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     * @return $this
     */
    public function setPrefix($prefix) {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvalidValue() {
        return $this->invalid_value;
    }

    /**
     * @param string $invalid_value
     * @return $this
     */
    public function setInvalidValue($invalid_value) {
        $this->invalid_value = $invalid_value;

        return $this;
    }
}