<?php
// remmber to run $ composer dump-autoload

require __DIR__.'/../vendor/autoload.php';

$sf_conn1 = new \KSDev\SafeCache\Adapter\Redis();
$conn2    = new Redis();
$conn2->connect('localhost');

$sf_conn1->invalidateKey('test');

$conn2->watch('test');
$v = $conn2->get('test');

$sf_conn1->getOrSet('test', function() {
    return 'One way of wrong value';
});

$conn2->multi();
$conn2->set('test', 'Another way of wrong data');
$conn2->exec();

// We would like to none of above is saved.

printf("safeCache value: %s\n", var_export($sf_conn1->get('test'), true));
printf("Real value: %s\n", $conn2->get('test'));

print( ($sf_conn1->get('test') === null && $conn2->get('test') === '__INVALID__') ? "Alt OK\n" : "IKKE OK!\n");